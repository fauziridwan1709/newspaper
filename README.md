# newspaper

studio teknologi kreatif flutter dev test application

## How to run apk

```
flutter run -t lib/main.dart
```

## How to build apk

```
flutter clean
flutter pub get
flutter build apk --release --no-shrink
```

## State management

Currently in love using state rebuilder for zero boilerplate state management
visit https://pub.dev/packages/states_rebuilder

## Architecture

using Reso coder's Flutter Clean Architecture. 
There are so much to learn about clean code. 
We can read it all on uncle Bob’s book.

![alt text](https://i0.wp.com/resocoder.com/wp-content/uploads/2019/08/Clean-Architecture-Flutter-Diagram.png?w=556&ssl=1)

## Rules
* use @deprecated before delete and update kind of things, like function, variable, etc.
* use BaseState in stateful widget
