import 'package:flutter/material.dart';
import 'package:newspaper/statesrebuilder/core/state/_states.dart';
import 'package:newspaper/statesrebuilder/core/theme/news_theme.dart';
import 'package:newspaper/statesrebuilder/features/news/presentation/pages/_pages.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Injector(
      inject: GlobalState.injectData,
      builder: (context) => MaterialApp(
        title: 'dev test',
        theme: ThemeData(
          primarySwatch: NewsTheme.primary,
        ),
        home: MainNews(),
      ),
    );
  }
}
