part of '_error.dart';

class NetworkException implements Exception {}

class BadRequestException implements Exception {
  String cause;
  int statusCode;
  BadRequestException({this.statusCode, this.cause});
}

class GeneralException implements Exception {
  String cause;
  GeneralException({this.cause});
}
