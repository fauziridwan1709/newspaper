part of '_error.dart';

abstract class Decide<T, K> {
  const Decide();
  B fold<B>(B Function(T failure) ifLeft, B Function(K result) ifRight);
  bool isLeft() => fold((_) => true, (_) => false);
  bool isRight() => fold((_) => false, (_) => true);
}

class Left<L, R> extends Decide<L, R> {
  final L _l;
  const Left(this._l);
  L get value => _l;

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifLeft(_l);
}

class Right<L, R> extends Decide<L, R> {
  final R _r;
  const Right(this._r);
  R get value => _r;

  @override
  B fold<B>(B Function(L t) ifLeft, B Function(R k) ifRight) => ifRight(_r);
}

abstract class Failure implements Exception {}

class NetworkFailure extends Failure {}

class NotFoundFailure extends Failure {}

class BadRequestFailure extends Failure {
  String cause;
  BadRequestFailure(this.cause);
}

class MaximumResultsReachedFailure extends Failure {
  String cause;
  MaximumResultsReachedFailure(this.cause);
}

class RateLimitedFailure extends Failure {
  String cause;
  RateLimitedFailure(this.cause);
}

class GeneralFailure extends Failure {
  String message;
  GeneralFailure(this.message);
}

class TimeoutFailure extends Failure {}
