import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:newspaper/statesrebuilder/core/screen/_screen.dart';
import 'package:newspaper/statesrebuilder/core/state/_states.dart';
import 'package:newspaper/statesrebuilder/core/widgets/_widgets.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

part 'base_state_rebuilder.dart';
part 'base_state.dart';
