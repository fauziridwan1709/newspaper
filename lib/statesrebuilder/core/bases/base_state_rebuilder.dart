part of '_base.dart';

///base state for states_rebuilder state management
abstract class BaseStateReBuilder<K extends FutureState<K, dynamic>> {
  void init();

  Widget buildAppBar(BuildContext context);

  Widget buildNarrowLayout(BuildContext context, ReactiveModel<K> snapshot,
      SizingInformation sizeInfo);

  Widget buildWideLayout(BuildContext context, ReactiveModel<K> snapshot,
      SizingInformation sizeInfo);

  Future<void> retrieveData();

  @protected
  Future<bool> onBackPressed();
}
