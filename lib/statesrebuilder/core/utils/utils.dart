part of '_utils.dart';

class Utils {
  ///logger for debug mode
  ///if return to production just comment the print method
  static void log(dynamic message, {String info}) {
    if (info != null) {
      print('--- $info ---');
    }
    print(message);
  }

  ///logger with couple dotes
  static void logWithDotes(dynamic message, {String info}) {
    log(message, info: info);
    log('.');
    log('.');
  }
}
