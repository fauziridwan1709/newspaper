part of '_client.dart';

///using http client with logger to make more clean code (no boilerplate)
Future<Response> getIt(String url, {Map<String, String> headers}) async {
  var resp = await client.get(url).timeout(GLOBAL_TIMEOUT);
  Utils.log(url, info: 'url');
  Utils.log(resp.statusCode, info: 'status code');
  Utils.logWithDotes(resp.body, info: 'data response');
  return resp;
}

Future<Response> postIt(String url, Map<String, dynamic> reqBody) async {
  var resp = await client
      .post(url, body: json.encode(reqBody))
      .timeout(GLOBAL_TIMEOUT);
  Utils.log(url, info: 'url');
  Utils.log(reqBody, info: 'body');
  Utils.logWithDotes(resp.body, info: 'data');
  return resp;
}

Future<Response> putIt(String url, Map<String, dynamic> reqBody) async {
  var resp =
      await client.put(url, body: json.encode(reqBody)).timeout(GLOBAL_TIMEOUT);
  Utils.log(url, info: 'url');
  Utils.log(reqBody, info: 'body');
  Utils.logWithDotes(resp.body, info: 'data');
  return resp;
}

Future<Response> deleteIt(String url) async {
  var resp = await client.delete(url).timeout(GLOBAL_TIMEOUT);
  Utils.log(url, info: 'url');
  Utils.logWithDotes(resp.body, info: 'data');
  return resp;
}
