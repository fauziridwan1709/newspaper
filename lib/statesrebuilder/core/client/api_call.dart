part of '_client.dart';

///this is simplification
///so we don't try catch on every method when calling remote data sources
Future<Decide<Failure, T>> apiCall<T>(Future<T> t) async {
  try {
    var futureCall = await t;
    return Right(futureCall);
  } on SocketException {
    return Left(NetworkFailure());
  } on TimeoutException {
    print('timeout');
    return Left(TimeoutFailure());
  } catch (e) {
    print(e);
    return Left(GeneralFailure(''));
  }
}
