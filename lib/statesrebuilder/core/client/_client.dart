import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:newspaper/statesrebuilder/core/constantvalue/constant_value.dart';
import 'package:newspaper/statesrebuilder/core/error/_error.dart';
import 'package:newspaper/statesrebuilder/core/utils/_utils.dart';
import 'package:http/http.dart';

part 'http.dart';
part 'api_call.dart';

var client = Client();
