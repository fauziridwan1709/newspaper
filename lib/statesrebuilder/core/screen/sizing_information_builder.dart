part of '_screen.dart';

///SizingInformationBuilder to use in baseState or personal use
class SizingInformationBuilder extends StatelessWidget {
  final Widget Function(
      BuildContext context, SizingInformation sizingInformation) builder;
  const SizingInformationBuilder({Key key, this.builder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(builder: (context, constraints) {
      var sizingInformation = SizingInformation(
        orientation: mediaQuery.orientation,
        deviceType: getDeviceScreenType(mediaQuery),
        localWidgetSize: Size(constraints.maxWidth, constraints.maxHeight),
        screenSize: mediaQuery.size,
      );
      return builder(context, sizingInformation);
    });
  }

  DeviceScreenType getDeviceScreenType(MediaQueryData mediaQuery) {
    var orientation = mediaQuery.orientation;
    double _deviceWidth;
    if (orientation == Orientation.landscape) {
      _deviceWidth = mediaQuery.size.height;
    } else {
      _deviceWidth = mediaQuery.size.width;
    }
    if (_deviceWidth > 600) {
      return DeviceScreenType.tablet;
    }
    return DeviceScreenType.mobile;
  }
}
