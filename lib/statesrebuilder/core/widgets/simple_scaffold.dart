part of '_widgets.dart';

class SimpleScaffold extends Scaffold {
  SimpleScaffold(
      {ScaffoldAttribute attr, PreferredSizeWidget appBar, Widget body})
      : super(
            key: attr.scaffoldKey,
            resizeToAvoidBottomInset: true,
            backgroundColor: attr.backgroundColor,
            floatingActionButton: attr.fab,
            floatingActionButtonLocation: attr.fabLocation,
            appBar: appBar,
            body: body);
}

class ScaffoldAttribute {
  GlobalKey<ScaffoldState> scaffoldKey;
  Color backgroundColor;
  Widget fab;
  FloatingActionButtonLocation fabLocation;

  ScaffoldAttribute(
      {this.scaffoldKey, this.backgroundColor, this.fab, this.fabLocation});
}
