part of '_widgets.dart';

///extending Container to create Simple Container Widget
class SContainer extends Container {
  SContainer({
    double width,
    double height,
    Widget child,
    Color color,
    EdgeInsetsGeometry padding = const EdgeInsets.all(0),
    EdgeInsetsGeometry margin,
    BorderRadius radius,
    BoxBorder border,
    BoxShape shape = BoxShape.rectangle,
    VoidCallback onTap,
    DecorationImage image,
    BoxFit fit,
    Gradient gradient,
  }) : super(
            width: width,
            height: height,
            margin: margin,
            padding: padding,
            decoration: BoxDecoration(
                gradient: gradient,
                shape: shape,
                image: image,
                color: color,
                borderRadius: radius,
                border: border),
            child: InkWell(
              onTap: onTap,
              child: Padding(padding: padding, child: child),
            ));
}
