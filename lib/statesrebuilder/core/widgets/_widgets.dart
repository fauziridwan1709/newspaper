import 'package:flutter/rendering.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:newspaper/statesrebuilder/core/error/_error.dart';
import 'package:newspaper/statesrebuilder/core/theme/news_theme.dart';

part 'simple_container.dart';
part 'view_error.dart';
part 'view_waiting.dart';
part 'simple_scaffold.dart';
part 'generic_list.dart';
part 'simple_text.dart';
