part of '_widgets.dart';

typedef ItemBuilder = Widget Function<T>(BuildContext context, int index, T t);

class GenericList<T> extends StatelessWidget {
  final int number;
  final List<T> list;
  final ItemBuilder builder;

  GenericList({@required this.number, this.list, this.builder})
      : assert(number != null);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return builder(context, index, T);
      },
    );
  }
}
