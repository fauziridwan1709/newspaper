part of '_widgets.dart';

enum NewsTextStyle { title, subtitle, description }

class SimpleText extends Text {
  SimpleText(String text,
      {double fontSize,
      Color color,
      NewsTextStyle style,
      String fontFamily,
      TextAlign textAlign,
      int maxLines})
      : super(text,
            maxLines: maxLines,
            textAlign: textAlign,
            style: _mapNewsTextStyle(style).copyWith(
                color: color, fontSize: fontSize, fontFamily: fontFamily));
}

TextStyle _mapNewsTextStyle(NewsTextStyle style) {
  if (style == NewsTextStyle.title) {
    return NewsTheme.boldBlack;
  } else if (style == NewsTextStyle.subtitle) {
    return NewsTheme.semiBlack;
  } else {
    return NewsTheme.descBlack;
  }
}
