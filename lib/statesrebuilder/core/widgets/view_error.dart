part of '_widgets.dart';

///error view when some failure occur in getting state
class ErrorView extends StatelessWidget {
  final dynamic error;
  ErrorView({@required this.error}) : assert(error != null);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return ListView(
      children: [
        SizedBox(height: width * .3),
        Image.asset(
          //todo object
          _mapFailureToAsset(error),
          width: width * 1,
          height: width * .65,
        ),
        SizedBox(height: 20),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
          child: SimpleText(
            _mapFailureToMessage(error),
            style: NewsTextStyle.subtitle,
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 40),
      ],
    );
  }

  ///mapping failure to asset
  ///this is representing the type of error
  String _mapFailureToAsset(Failure failure) {
    switch (failure.runtimeType) {
      case NetworkFailure:
        return 'assets/images/no_internet.png';
      case TimeoutFailure:
        return 'assets/images/timeout.png';
      case BadRequestFailure:
        return 'assets/images/bad_request.png';
      case MaximumResultsReachedFailure:
        return 'assets/images/max.png';
      case RateLimitedFailure:
        return 'assets/images/limit.png';
      default:
        print(failure.runtimeType);
        return 'assets/images/general.png';
    }
  }

  ///mapping failure to message, to deliver more info about the error
  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case NetworkFailure:
        return 'No Internet Connection';
      case TimeoutFailure:
        return 'Timeout, try again';
      case BadRequestFailure:
        return (failure as BadRequestFailure).cause;
      case MaximumResultsReachedFailure:
        return (failure as MaximumResultsReachedFailure).cause;
      case RateLimitedFailure:
        return (failure as RateLimitedFailure).cause;
      default:
        print(failure.runtimeType);
        return 'Unexpected error';
    }
  }
}
