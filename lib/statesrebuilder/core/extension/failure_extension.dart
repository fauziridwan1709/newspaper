part of '_extension.dart';

@deprecated
extension FailureExtension on Failure {
  String translate() {
    if (this is NetworkFailure) {
      return 'no Internet, try again';
    } else if (this is TimeoutFailure) {
      return 'timeout, try again';
    } else {
      return 'server error';
    }
  }
}
