part of '_extension.dart';

extension StringExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${substring(1)}';
  String get allInCaps => toUpperCase();
  String get capitalizeFirstOfEach => this != null
      ? split(' ')
          .map((word) => word != ''
              ? (word.trimLeft()[0].toUpperCase() +
                  word.substring(1).toLowerCase())
              : '')
          .join(' ')
      : this;
  String get capitalize => this[0].toUpperCase() + substring(1);
  String get showNarration => isEmpty ? 'Tidak ada deskripsi' : this;
  int get toInteger => int.parse(this);
}
