part of '_extension.dart';

@deprecated
extension ParsedExtension<T> on Parsed<T> {
  Failure toFailure() {
    return statusCode.toFailure(message);
  }
}
