part of '_extension.dart';

extension DateTimeExtension on DateTime {
  String get timeInString => toIso8601String().substring(0, 19);
}
