part of '_extension.dart';

extension IntegerExtension on int {
  bool get isSuccessOrCreated => this == SUCCESS || this == CREATED;
  bool get isBadRequest => this == BAD_REQUEST;
  bool get isNotFound => this == NOT_FOUND;
  bool get isMaximumResultsReached => this == MAX_REACHED;
  bool get isRateLimit => this == RATE_LIMITED;
  bool get isZero => this == 0;

  ///translate [statusCode]
  B translate<B>(B Function() ifSuccess, B Function() ifElse) {
    if (isSuccessOrCreated) {
      return ifSuccess();
    } else {
      return ifElse();
    }
  }

  ///extension untuk [statusCode]
  Failure toFailure(String message) {
    //todo add more failure
    if (isBadRequest) {
      return BadRequestFailure(message);
    } else if (isNotFound) {
      return NotFoundFailure();
    } else if (isMaximumResultsReached) {
      return MaximumResultsReachedFailure(message);
    } else if (isRateLimit) {
      return RateLimitedFailure(message);
    } else {
      return GeneralFailure(message);
    }
  }
}
