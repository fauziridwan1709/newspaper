part of '_extension.dart';

class Parsed<T> {
  int statusCode;
  int totalResult;
  String message;
  T data;

  Parsed.fromJson(Map<String, dynamic> json, int statusCode, T type) {
    this.statusCode = statusCode;
    totalResult = json['totalResults'];
    message = json['message'];
    data = type;
  }
}

///semua response di generalisir kedalam object parsed
extension ResponseExtension<T extends Models> on Response {
  // ignore: avoid_shadowing_type_parameters
  Parsed<T> parse<T>(T t) =>
      Parsed.fromJson(json.decode(body) as Map<String, dynamic>, statusCode, t);

  Map<String, dynamic> get bodyAsMap =>
      json.decode(body) as Map<String, dynamic>;
}
