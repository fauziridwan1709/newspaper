part of '_extension.dart';

extension BoolExtension on bool {
  double get opacity => this ? 1.0 : 0.0;
}
