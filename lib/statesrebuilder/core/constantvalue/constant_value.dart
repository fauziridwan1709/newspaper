const int TIMEOUT_EXCEPTION_STATUS_CODE = 501;
const int NO_INTERNET_STATUS_CODE = 503;
const int GENERAL_STATUS_CODE = 500;
const int TOO_MANY_REQUEST = 429;
const int BAD_REQUEST = 400;
const int NOT_FOUND = 404;
const int SUCCESS = 200;
const int CREATED = 201;
const int MAX_REACHED = 426;
const int RATE_LIMITED = 429;
const Duration GLOBAL_TIMEOUT = Duration(seconds: 15);

const String baseUrl = 'https://newsapi.org/v2';
// const String apiKey = 'e2e2806d9e13466ca826020fabc551e1';
const String apiKey = '2b1ae73b5fd34248bdae784aaac45083';
