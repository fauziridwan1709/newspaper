import 'package:flutter/material.dart';

///theme for this app
class NewsTheme {
  const NewsTheme._privateConstructor();

  static MaterialColor primary = MaterialColor(
    0xFF7BA939,
    <int, Color>{
      50: Color(0xFF000000),
      100: Color(0xFF000000),
      200: Color(0xFF000000),
      300: Color(0xFF000000),
      400: Color(0xFF000000),
      500: Color(0xFF000000),
      600: Color(0xFF000000),
      700: Color(0xFF000000),
      800: Color(0xFF000000),
      900: Color(0xFF000000),
    },
  );

  static const Color transparent = Colors.transparent;
  static const Color white = Colors.white;
  static const Color black = Colors.black;
  static const Color lightGray = Color(0xFFEEEEEE);

  static const TextStyle descBlack = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.normal,
      fontFamily: 'OpenSans');

  static const TextStyle mediumBlack = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w500,
      fontFamily: 'OpenSans');

  static const TextStyle semiBlack = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w600,
      fontFamily: 'OpenSans');

  static const TextStyle boldBlack = TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w700,
      fontFamily: 'OpenSans');
}
