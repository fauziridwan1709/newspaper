part of '_states.dart';

///GlobalState, an interface to call reactive model
class GlobalState {
  GlobalState._();

  //todo
  static var injectData = <Injectable>[
    Inject(() => NewsState()),
  ];

  // @override
  static ReactiveModel<T> instance<T>() {
    return Injector.getAsReactive<T>();
  }

  static T state<T>() {
    return Injector.getAsReactive<T>().state;
  }

  static Future<void> setState<T>(
    Function(T) func, {
    void Function(BuildContext context) onSetState,
    void Function(BuildContext context) onRebuildState,
    void Function(BuildContext context, dynamic error) onError,
    void Function(BuildContext context, T model) onData,
  }) async {
    return Injector.getAsReactive<T>().setState((T s) => func,
        onSetState: onSetState,
        onData: onData,
        onError: onError,
        onRebuildState: onRebuildState);
  }

  static ReactiveModel<NewsState> news() {
    return Injector.getAsReactive<NewsState>();
  }
}
