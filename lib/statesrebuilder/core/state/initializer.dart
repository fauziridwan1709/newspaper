part of '_states.dart';

enum InitializerDelay {
  min1,
  min5,
}

///initializer with refresh indicatorKey
///ketika di inisialisasi maka initializer akan mengecek state dan cachekey
///apakah diperlukan untuk melakukan fetch data
class Initializer {
  ReactiveModel rm;
  String ck;
  GlobalKey<RefreshIndicatorState> rIndicator;
  bool state;
  InitializerDelay delay;

  Initializer(
      {@required ReactiveModel reactiveModel,
      @required String cacheKey,
      @required this.rIndicator,
      @required this.state,

        ///default delay fetch data secara otomatis adalah 5 menit.
        ///lakukan pull to refresh bila diperlukan fetch data secara manual
      this.delay = InitializerDelay.min5})
      : assert(reactiveModel != null),
        assert(cacheKey != '' && cacheKey != null),
        assert(rIndicator != null),
        assert(state != null),
        rm = reactiveModel,
        ck = cacheKey;

  void initialize() {
    SharedPreferences.getInstance().then((value) async {
      if (value.containsKey(ck)) {
        var date = value.getString(ck);
        if (isDifferenceLessThanFiveMin(date) && state) {
        } else {
          await value.setString(ck, DateTime.now().toIso8601String());
          await rIndicator.currentState.show();
        }
      } else {
        await value.setString(ck, DateTime.now().toIso8601String());
        await rIndicator.currentState.show();
      }
    });
  }
}

///use
bool _getDelayTime(InitializerDelay delay, String date) {
  if (delay == InitializerDelay.min1) {
    return isDifferenceLessThanMin(date);
  } else if (delay == InitializerDelay.min5) {
    return isDifferenceLessThanFiveMin(date);
  } else {
    throw UnimplementedError();
  }
}

bool isDifferenceLessThanFiveMin(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 5);
}

bool isDifferenceLessThanMin(String time) {
  return DateTime.now().difference(DateTime.parse(time)) < Duration(minutes: 1);
}
