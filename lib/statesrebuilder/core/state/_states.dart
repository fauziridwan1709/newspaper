import 'package:flutter/material.dart';
import 'package:newspaper/statesrebuilder/features/news/presentation/states/_states.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

part 'future_state.dart';
part 'global_state.dart';
part 'cleaner.dart';
part 'initializer.dart';
