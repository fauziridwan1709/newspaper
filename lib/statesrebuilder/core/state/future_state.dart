part of '_states.dart';

///as a tag for the reactive model where contains an async method
abstract class FutureState<T, K> {
  ///cache key to check last fecth time base on this cache key, every reactive model must define they own key
  String cacheKey;

  ///check the condition, apabila pada suatu state tidak terpenuhi maka akan melakukan fetch
  ///terlepas dari pengaturan fetch time (5 menit sekali / 1 menit sekali)
  bool getCondition();

  Future<void> retrieveData(K k);
}
