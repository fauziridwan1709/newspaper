part of '_widgets.dart';

class CardNews extends StatelessWidget {
  final Article article;
  final Size size;
  final VoidCallback onTap;

  const CardNews({@required this.article, @required this.size, this.onTap})
      : assert(article != null, 'Article cannot be null'),
        assert(size != null, 'size cannot be null');

  @override
  Widget build(BuildContext context) {
    return SContainer(
      onTap: () => navigate(context, DetailNews(article: article, size: size)),
      height: size.width * .3,
      padding: EdgeInsets.symmetric(
          vertical: size.width * .02, horizontal: size.width * .04),
      child: Row(
        children: [
          SContainer(
            width: size.width * .25,
            height: size.width * .3,
            image: DecorationImage(
                image: NetworkImage(article.urlToImage ?? ''),
                fit: BoxFit.cover),
            radius: BorderRadius.circular(8),
          ),
          SizedBox(width: size.width * .05),
          Expanded(
            child: Column(
              children: [
                SimpleText(
                  article.title,
                  maxLines: 2,
                  style: NewsTextStyle.title,
                  fontSize: size.width / 28,
                ),
                SimpleText(
                  article.description,
                  maxLines: 3,
                  style: NewsTextStyle.description,
                  fontSize: size.width / 34,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
