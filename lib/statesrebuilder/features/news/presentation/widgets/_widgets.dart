import 'package:flutter/material.dart';
import 'package:newspaper/statesrebuilder/core/navigator/navigator.dart';
import 'package:newspaper/statesrebuilder/core/state/_states.dart';
import 'package:newspaper/statesrebuilder/core/theme/news_theme.dart';
import 'package:newspaper/statesrebuilder/core/utils/_utils.dart';
import 'package:newspaper/statesrebuilder/core/widgets/_widgets.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';
import 'package:newspaper/statesrebuilder/features/news/presentation/pages/_pages.dart';

part 'loader.dart';
part 'card_news.dart';
part 'head.dart';
part 'author.dart';
part 'publish_time.dart';
