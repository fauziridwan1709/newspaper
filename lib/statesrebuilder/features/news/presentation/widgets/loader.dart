part of '_widgets.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
          padding: EdgeInsets.all(10), child: CircularProgressIndicator()),
    );
  }
}
