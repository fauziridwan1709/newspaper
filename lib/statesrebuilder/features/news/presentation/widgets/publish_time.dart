part of '_widgets.dart';

class PublishTime extends StatelessWidget {
  final String publishTime;
  final Size size;

  const PublishTime({this.publishTime, this.size});
  @override
  Widget build(BuildContext context) {
    var date = DateTime.parse(publishTime).toLocal();
    Utils.log(publishTime, info: 'publish time');
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: size.width * .05),
      child: SimpleText(
        '${date.day} ${bulanFull[date.month - 1]} ${date.year}',
        style: NewsTextStyle.subtitle,
        fontSize: size.width / 28,
      ),
    );
  }
}

var bulanFull = <String>[
  'Januari',
  'Februari',
  'Maret',
  'April',
  'Mei',
  'Juni',
  'Juli',
  'Agustus',
  'September',
  'Oktober',
  'November',
  'Desember'
];
