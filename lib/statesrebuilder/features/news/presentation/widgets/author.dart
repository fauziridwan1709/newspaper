part of '_widgets.dart';

class Author extends StatelessWidget {
  final String authorsName;
  final Size size;

  const Author({this.authorsName, this.size});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.width * .05, vertical: size.width * .05),
      child: Row(
        children: [
          SimpleText('Author: ',
              style: NewsTextStyle.title, fontSize: size.width / 30),
          SizedBox(width: 10),
          SContainer(
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            color: NewsTheme.lightGray,
            radius: BorderRadius.circular(8),
            child: SimpleText(
              authorsName,
              style: NewsTextStyle.subtitle,
              fontSize: size.width / 28,
            ),
          ),
        ],
      ),
    );
  }
}
