part of '_widgets.dart';

class Head extends StatelessWidget {
  final VoidCallback onShowAll;
  final Size size;

  const Head({this.onShowAll, this.size});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.width * .05, vertical: size.width * .04),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SimpleText(
            'Everything',
            style: NewsTextStyle.title,
            fontSize: size.width / 18,
          ),
          SContainer(
            color: NewsTheme.lightGray,
            radius: BorderRadius.circular(6.0),
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 8),
            child: SimpleText(
              '${GlobalState.news().state.query}',
              style: NewsTextStyle.subtitle,
              fontSize: size.width / 30,
            ),
          )
        ],
      ),
    );
  }
}
