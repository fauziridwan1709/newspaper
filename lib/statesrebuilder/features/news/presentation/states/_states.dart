import 'package:newspaper/statesrebuilder/core/state/_states.dart';
import 'package:newspaper/statesrebuilder/core/utils/_utils.dart';
import 'package:newspaper/statesrebuilder/features/news/data/repositories/_repositories.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';
import 'package:newspaper/statesrebuilder/core/extension/_extension.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/repositories/_repositories.dart';

part 'news_state.dart';
