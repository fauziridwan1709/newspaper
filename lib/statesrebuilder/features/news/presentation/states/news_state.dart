part of '_states.dart';

class NewsState implements FutureState<NewsState, int> {
  final NewsRepository repo = NewsRepositoryImpl();

  List<Article> articles;
  int page = 1;
  bool hasReachedMax = false;

  ///default query
  String query = 'iphone';

  @override
  String cacheKey = 'time_list_news';

  @override
  bool getCondition() {
    return articles != null;
  }

  @override
  Future<void> retrieveData(int page) async {
    var resp = await repo.getAllArticle(page, query);
    resp.fold((failure) {
      Utils.log(failure);
      throw failure;
    }, (result) {
      result.statusCode.translate(() {
        articles = result.data;
        if (result.data.length < 10) {
          hasReachedMax = true;
        } else {
          hasReachedMax = false;
        }
        Utils.log('success', info: 'get all news');
      }, () {
        throw result.statusCode.toFailure(result.message);
      });
    });
  }

  Future<void> loadMore() async {
    page += 1;
    print(page);
    var resp = await repo.getAllArticle(page, query);
    resp.fold((failure) {
      throw failure;
    }, (result) {
      result.statusCode.translate(() {
        articles..addAll(result.data);
        if (result.data.length < 10) {
          hasReachedMax = true;
        } else {
          hasReachedMax = false;
        }
        Utils.log('success', info: 'get all news');
      }, () {
        throw result.statusCode.toFailure(result.message);
      });
    });
  }

  void changeQuery(String q) {
    query = q;
  }
}
