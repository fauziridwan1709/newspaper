import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:newspaper/statesrebuilder/core/bases/_base.dart';
import 'package:newspaper/statesrebuilder/core/navigator/navigator.dart';
import 'package:newspaper/statesrebuilder/core/screen/_screen.dart';
import 'package:newspaper/statesrebuilder/core/state/_states.dart';
import 'package:newspaper/statesrebuilder/core/theme/news_theme.dart';
import 'package:newspaper/statesrebuilder/core/utils/_utils.dart';
import 'package:newspaper/statesrebuilder/core/widgets/_widgets.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';
import 'package:newspaper/statesrebuilder/features/news/presentation/states/_states.dart';
import 'package:newspaper/statesrebuilder/features/news/presentation/widgets/_widgets.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

part 'main_news.dart';
part 'detail_news.dart';
part 'webview_article.dart';
part 'news_search_delegate.dart';
