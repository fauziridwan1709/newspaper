part of '_pages.dart';

class WebViewArticle extends StatefulWidget {
  WebViewArticle({this.url, this.title, this.size});
  final String title;
  final String url;
  final Size size;
  @override
  _WebViewArticleState createState() => _WebViewArticleState();
}

class _WebViewArticleState extends State<WebViewArticle> {
  InAppWebViewController webView;
  double progress = 0;
  String title = '';
  String link;
  final ChromeSafariBrowser browser =
      ChromeSafariBrowser(bFallback: InAppBrowser());

  @override
  void initState() {
    super.initState();
    link = widget.url;
    title = widget.title;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => pop(context),
          color: NewsTheme.black,
        ),
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: RichText(
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          text: TextSpan(
              text: widget.title,
              style: NewsTheme.descBlack
                  .copyWith(fontSize: widget.size.width / 22)),
        ),
      ),
      body: Container(
          child: Column(children: <Widget>[
        Container(
            child: progress < 1.0
                ? LinearProgressIndicator(value: progress)
                : Container()),
        Expanded(
          child: Container(
            child: InAppWebView(
              initialUrl: link,
              initialHeaders: {},
              onLoadError: (InAppWebViewController controller, String url,
                  int code, String message) async {},
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                    cacheEnabled: false,
                    clearCache: true,
                    debuggingEnabled: true,
                    userAgent:
                        'Mozilla/5.0 (Linux; Android 7.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/86.0.4240.185 Mobile Safari/535.19'),
              ),
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onLoadStart: (InAppWebViewController controller, String url) {
                Utils.log(url, info: 'load start url');
              },
              onLoadStop:
                  (InAppWebViewController controller, String url) async {
                Utils.log(url, info: 'load stop url');
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(() {
                  this.progress = progress / 100;
                });
              },
            ),
          ),
        ),
      ])),
    );
  }
}
