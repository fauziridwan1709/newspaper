part of '_pages.dart';

class DetailNews extends StatelessWidget {
  final Article article;
  final Size size;

  const DetailNews({this.article, this.size});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: size.width * .7,
            pinned: false,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: EdgeInsets.all(size.width * .05),
              title: SimpleText(
                article.title,
                style: NewsTextStyle.title,
                fontFamily: 'Chomsky',
                color: NewsTheme.white,
                fontSize: size.width / 26,
              ),
              background: SContainer(
                width: size.width,
                height: 300,
                image: DecorationImage(
                    image: NetworkImage(article.urlToImage), fit: BoxFit.cover),
                child: SContainer(
                  gradient: LinearGradient(colors: [
                    NewsTheme.black.withOpacity(.8),
                    NewsTheme.transparent
                  ], begin: Alignment.bottomCenter, end: Alignment.topCenter),
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Author(size: size, authorsName: article.author),
              PublishTime(size: size, publishTime: article.publishedAt),
              SContainer(
                margin: EdgeInsets.all(size.width * .05),
                child: SimpleText(
                  article.content,
                  style: NewsTextStyle.subtitle,
                  fontSize: size.width / 30,
                ),
              ),
              Center(
                child: SContainer(
                  onTap: () => navigate(
                      context,
                      WebViewArticle(
                          url: article.url, title: article.title, size: size)),
                  padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  color: NewsTheme.lightGray,
                  radius: BorderRadius.circular(8),
                  child: SimpleText(
                    'Show full article in Webview here',
                    style: NewsTextStyle.subtitle,
                    fontSize: size.width / 28,
                  ),
                ),
              )
            ]),
          )
        ],
      ),
    );
  }
}
