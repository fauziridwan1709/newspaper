part of '_pages.dart';

class NewsSearchDelegate extends SearchDelegate<Article> {
  final GlobalKey<RefreshIndicatorState> refreshIndicator;

  NewsSearchDelegate({this.refreshIndicator});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    ///some hacks :) but this was not cool
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      close(context, null);
      var newsState = GlobalState.news();
      newsState.setState((s) => s.changeQuery(query));
      refreshIndicator.currentState.show();
    });
    return SContainer();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }
}
