part of '_pages.dart';

///presenting list of news page, default query is q=iphone
///

class MainNews extends StatefulWidget {
  @override
  _MainNewsState createState() => _MainNewsState();
}

class _MainNewsState extends BaseState<MainNews, NewsState> {
  final _scrollController = ScrollController();
  final newsState = GlobalState.news();
  var completer = Completer<Null>();

  bool _visible;

  @override
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  @override
  void init() {
    _scrollController.addListener(_onScroll);
    _visible = true;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Future<void> retrieveData() async {
    if (newsState.isWaiting) {
      setState(() => _visible = false);
    }
    await newsState.setState((s) => s.retrieveData(1),
        onData: (context, data) => setState(
              () => _visible = false,
            ),
        onRebuildState: (_) => setState(() => _visible = true));
  }

  @override
  ScaffoldAttribute buildAttribute() {
    return ScaffoldAttribute();
  }

  @override
  Widget buildAppBar(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return AppBar(
      backgroundColor: NewsTheme.white,
      shadowColor: Colors.transparent,
      title: SimpleText(
        'Newspaper',
        style: NewsTextStyle.title,
        fontSize: width / 22,
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.search),
          color: NewsTheme.black,
          onPressed: () => showSearch<Article>(
            context: context,
            delegate: NewsSearchDelegate(refreshIndicator: refreshIndicatorKey),
          ),
        )
      ],
    );
  }

  ///build layout untuk mobile
  @override
  Widget buildNarrowLayout(BuildContext context,
      ReactiveModel<NewsState> snapshot, SizingInformation sizeInfo) {
    var screenSize = sizeInfo.screenSize;
    return RefreshIndicator(
        child: WhenRebuilder<NewsState>(
          observe: () => snapshot,
          onIdle: () => WaitingView(),
          onWaiting: () => WaitingView(),
          onError: (dynamic error) => ErrorView(error: error),
          onData: (data) {
            if (data.articles.isEmpty) {
              return Center(child: SimpleText('maaf, empty'));
            }
            return AnimatedOpacity(
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 450),
              child: ListView.builder(
                controller: _scrollController,
                itemCount: data.hasReachedMax
                    ? data.articles.length + 1
                    : data.articles.length + 2,
                itemBuilder: (_, index) {
                  if (index == 0) {
                    return Head(
                      // onShowAll: () => navigate,
                      size: screenSize,
                    );
                  } else if (index - 1 == data.articles.length) {
                    return Loader();
                  }
                  return CardNews(
                      article: data.articles.elementAt(index - 1),
                      size: screenSize);
                },
              ),
            );
          },
        ),
        key: refreshIndicatorKey,
        onRefresh: retrieveData);
  }

  @override
  Widget buildWideLayout(BuildContext context,
      ReactiveModel<NewsState> snapshot, SizingInformation sizeInfo) {
    //todo build tablet layout here
    return Container();
  }

  @override
  Future<bool> onBackPressed() async {
    return true;
  }

  Future<void> _onScroll() async {
    if (_isBottom && await !completer.isCompleted) {
      completer.complete();
      await newsState.state.loadMore().then((_) {
        newsState.notify();
        completer = Completer<Null>();
      });
    }
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) {
      return false;
    }
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
