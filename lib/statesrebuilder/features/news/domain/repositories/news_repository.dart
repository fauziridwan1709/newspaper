part of '_repositories.dart';

abstract class NewsRepository {
  Future<Decide<Failure, Parsed<List<Article>>>> getAllArticle(
      int page, String q);
}
