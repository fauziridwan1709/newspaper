part of '_datasources.dart';

abstract class NewsRemoteDataSource {
  Future<Parsed<List<Article>>> getAllArticle(int page, String q);
}

class NewsRemoteDataSourceImpl implements NewsRemoteDataSource {
  @override
  Future<Parsed<List<Article>>> getAllArticle(int page, String q) async {
    var url =
        '$baseUrl/everything?qInTitle=$q&page=$page&pageSize=10&apiKey=$apiKey';
    var resp = await getIt(url);
    var data = resp.parse(ListArticleModel.fromJson(resp.bodyAsMap).list);
    return data;
  }
}
