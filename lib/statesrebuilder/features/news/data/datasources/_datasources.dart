import 'dart:async';
import 'dart:io';

import 'package:newspaper/statesrebuilder/core/client/_client.dart';
import 'package:newspaper/statesrebuilder/core/constantvalue/constant_value.dart';
import 'package:newspaper/statesrebuilder/core/extension/_extension.dart';
import 'package:newspaper/statesrebuilder/core/utils/_utils.dart';
import 'package:newspaper/statesrebuilder/features/news/data/models/_models.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';

part 'news_remote_data_source.dart';
