part of '_models.dart';

class ListArticleModel extends Models {
  List<ArticleModel> list;

  ListArticleModel({this.list});

  ListArticleModel.fromJson(Map<String, dynamic> json) {
    if (json['articles'] != null) {
      list = <ArticleModel>[];
      json['articles'].forEach((dynamic v) {
        list.add(ArticleModel.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (list != null) {
      data['articles'] = list.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
