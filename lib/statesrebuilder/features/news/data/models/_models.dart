import 'package:newspaper/statesrebuilder/core/models/_models.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';

part 'article_model.dart';
part 'source_model.dart';
part 'list_article_model.dart';
