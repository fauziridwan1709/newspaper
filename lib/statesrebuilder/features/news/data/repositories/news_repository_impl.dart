part of '_repositories.dart';

class NewsRepositoryImpl implements NewsRepository {
  final NewsRemoteDataSource remoteDataSource = NewsRemoteDataSourceImpl();

  @override
  Future<Decide<Failure, Parsed<List<Article>>>> getAllArticle(
      int page, String q) async {
    print('api call');
    return await apiCall(remoteDataSource.getAllArticle(page, q));
  }
}
