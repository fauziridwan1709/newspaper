import 'package:newspaper/statesrebuilder/core/client/_client.dart';
import 'package:newspaper/statesrebuilder/core/error/_error.dart';
import 'package:newspaper/statesrebuilder/core/extension/_extension.dart';
import 'package:newspaper/statesrebuilder/features/news/data/datasources/_datasources.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/entities/_entities.dart';
import 'package:newspaper/statesrebuilder/features/news/domain/repositories/_repositories.dart';

part 'news_repository_impl.dart';
